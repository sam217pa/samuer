% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/gc.R
\name{gc}
\alias{gc}
\title{GC content of string}
\usage{
gc(x)
}
\description{
Returns the GC content of a Biostring-parseable object as a
percentage.
}
