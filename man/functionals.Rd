% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/functionals.R
\name{delist}
\alias{delist}
\alias{map}
\alias{map_dbl}
\alias{map_chr}
\alias{map_lgl}
\alias{map2}
\alias{map2_dbl}
\alias{sow}
\alias{sow2}
\alias{pile}
\alias{emap2}
\alias{emap}
\alias{nmap}
\alias{namap}
\alias{lfdf}
\alias{map_df}
\alias{flt_by}
\alias{concat}
\alias{walk}
\alias{walk2}
\alias{walkn}
\alias{pmap}
\title{Unlist that unname its output}
\usage{
delist(...)

map(.lst, .f, ...)

map_dbl(...)

map_chr(...)

map_lgl(...)

map2(.lst1, .lst2, .f, ...)

map2_dbl(...)

sow(.lst, .f)

sow2(.lst1, .lst2, .f)

pile(x, ...)

emap2(.lst1, .lst2, .f, ...)

emap(.lst, .f, ...)

nmap(.lst, .f)

namap(.lst, .f)

lfdf(x)

map_df(.lst, .f)

flt_by(.lst, .is_f)

concat(str, sep = ",")

walk(X, FUN, ...)

walk2(X, Y, FUN, ...)

walk2(X, Y, FUN, ...)

walkn(..., FUN)

pmap(X, FUN, ...)
}
\description{
.f should take two arguments

Rbind each data.frame element of a list and add a column
referencing the list element name.

Apply .f to .lst and return a data.frame.
.f to .lst[[1]] should return a data.frame element than can be piled up.

Returns each element of .lst for which the application of .is_f
returns TRUE.
}
