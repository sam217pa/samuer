.PHONY: all

RCMD = R CMD BATCH --no-save --no-restore
DEPS = $(shell ls -1 R/*.R)

all: $(DEPS)
	Rscript "build.R"
