## * depth

#' Read coverage analysis by samtools
#'
#' Parse the output of \code{samtools depth} and returns a \code{tibble}
#' with position and coverage. The returned object is a \code{coverage}
#' object with associated methods.
#'
#' @importFrom tibble as_tibble
#' @importFrom data.table fread
#' @importFrom dplyr select
#'
#' @export

samtools_coverage <- function(filename) {
  dat <- data.table::fread(filename) %>%
    tibble::as_tibble() %>%
    set_colnames(cs("name, pos, cov")) %>%
    dplyr::select(-name)
  class(dat) <- c("coverage", class(dat))
  dat
}

#' Plot method for coverage class
#'
#' Returns a coverage plot using ggplot2.
#'
#' @importFrom dplyr sample_n
#' @importFrom ggplot2 ggplot geom_point labs
#'
#' @export

plot.coverage <- function(x, y, ...) {
  x %>%
    dplyr::sample_n(1e5) %>%
    ggplot2::ggplot(aes(x = pos, y = cov)) +
    ggplot2::geom_point(alpha = 0.1, size = 0.1) +
    scale_x_megabase() +
    ggplot2::labs(x = "Position on Genome [Mb]",
         y = "Genome Coverage")
}
