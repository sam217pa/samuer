#' Pretty LaTeX output
#'
#' @importFrom knitr kable
#'
#' @export

latex_table<- function(x, ...){
  print(knitr::kable(
    x, format = "latex",
    align = "c",
    booktabs = TRUE,
    digits = 2, ...))
}
