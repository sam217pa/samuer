#' Better dimensions to ggsave
#'
#' Save plot as a perfect rectangular form according to the golden
#' ratio.
#'
#' @param filename (chr) the output plot destination. Will be written
#'   to figures/filename, the figures directory will be created if not
#'   already present in current working directory.
#' @param hd (lgl) TRUE by default, save as a high resolution figure.
#' @param sd (lgl) save as a standard resolution figure.
#'
#' @importFrom ggplot2 ggsave
#' @export

fig_save <- function(filename, ..., hd=TRUE, sd=NULL) {
  if (! dir.exists("figures")) {
    dir.create("figures")
  }
  if ((! missing(sd)) & is.logical(sd)) {
    hd <- ifelse(sd, FALSE, TRUE)
  }
  ggsave(paste0("figures/", filename), ... ,
         width = ifelse(hd, 10, 5),
         height = ifelse(hd, 6.18, 3.09))
}

##' Save output as a4 format
##'
##'
##' @param output the file outputed
##' @param landscape lgl, wether to print in landscape or not.
##' @return nothing
##' @author Samuel Barreto
##' @importFrom ggplot2 ggsave
##' @export

save_as_a4 <- function(output=NULL, landscape=TRUE) {
  if (landscape)
    ggsave(file=output, width = 297, height = 210, units = "mm")
  else
    ggsave(file=output, width = 210, height = 297, units = "mm")
}

##' Save output in a3 format
##'
##' @param landscape (lgl) Wether to print in landscape or not.
##' @return nothing
##' @author Samuel Barreto
##' @importFrom ggplot2 ggsave
##' @seealso [ggplot2]ggsave
##' @export

save_as_a3 <- function(..., landscape = TRUE) {
  .d <- c(420, 297)
  if (!landscape)
    .d <- rev(.d)
  ggsave(..., width = .d[1], height = .d[2], units = "mm")
}

##' Print object size in Mb
##'
##' @param object the object to print
##' @return its memory usage in Mega bases
##' @author Samuel Barreto
##' @export

object_size_pretty <- function(object) {
  print(object.size(object), units = "Mb")
}
